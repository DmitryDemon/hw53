import React, { Component } from 'react';
import './TodoList.css';
import TodoItems from './TodoItems'

class TodoList extends Component{
    constructor(props){
        super(props);

        this.state ={
            items: []
        };

        this.addItem = this.addItem.bind(this);
        this.deleteItems = this.deleteItems.bind(this);
    }

    addItem(e){
        if (this._inputElem !==''){
            const newItem = {
                text: this._inputElem.value,
                key : Date.now()
            };
            this.setState((prevState) => {
                return{
                    items: prevState.items.concat(newItem)
                }
            })
        }
        this._inputElem.value = '';
        console.log(this.state.items);
        e.preventDefault();
    }
    deleteItems = key =>{
        const items = [...this.state.items];
        const index = items.findIndex(task => {
            return (
                task.id === key
            )
        });
        items.splice(index,1);
        this.setState({items})
    };


    render() {
        return(
            <div className='todoList'>
                <div className='header'>
                    <form onSubmit={this.addItem}>
                        <input
                            ref={(a)=>this._inputElem = a}
                            placeholder='Add new task'
                        />
                        <button type='submit'>Add</button>
                    </form>
                </div>
                <TodoItems entries={this.state.items}
                            delete={(key) => this.deleteItems(key)}/>
            </div>
        )
    }
}
export default TodoList;
